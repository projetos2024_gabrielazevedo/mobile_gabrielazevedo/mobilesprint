import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native';

const Cadastro = ({ navigation }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const handleSignup = () => {
    if (username && password && confirmPassword && password === confirmPassword) {

      setErrorMessage('Usuário cadastrado com sucesso');
      navigation.navigate('Schedule');
    } else {
      setErrorMessage('Por favor, preencha todos os campos corretamente');
    }
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => navigation.navigate('Home')} style={styles.homeButton}>
        <Text style={styles.homeButtonText}> ⇦ Voltar para o Início</Text>
      </TouchableOpacity>
      <Text style={styles.title}>Página de Cadastro</Text>
      <TextInput
        style={styles.input}
        placeholder="Nome de usuário"
        value={username}
        onChangeText={setUsername}
      />
      <TextInput
        style={styles.input}
        placeholder="Senha"
        secureTextEntry={true}
        value={password}
        onChangeText={setPassword}
      />
      <TextInput
        style={styles.input}
        placeholder="Confirmar senha"
        secureTextEntry={true}
        value={confirmPassword}
        onChangeText={setConfirmPassword}
      />
      <TouchableOpacity style={styles.button} onPress={handleSignup}>
        <Text style={styles.buttonText}>Cadastrar</Text>
      </TouchableOpacity>
      {errorMessage ? <Text style={styles.errorMessage}>{errorMessage}</Text> : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    backgroundColor: '#CAF0F8',
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
    fontWeight: 'bold', 
    color: '#023E8A',
  },
  input: {
    width: '80%', 
    height: 40,
    backgroundColor: 'white',
    borderColor: '#023E8A',
    borderWidth: 2,
    marginBottom: 10,
    paddingHorizontal: 10,
    borderRadius: 8, 
  },
  button: {
    backgroundColor: '#90E0EF',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 6,
    marginBottom: 10,
    width: '80%',
  },
  buttonText: {
    fontSize: 18,
    color: '#023E8A',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  homeButton: {
    position: 'absolute',
    top: 50, 
    left: 20,
  },
  homeButtonText: {
    color: '#023E8A',
    fontSize: 16,
    fontWeight: 'bold',
  },
  errorMessage: {
    color: 'black',
    marginTop: 10,
  },
});

export default Cadastro;
