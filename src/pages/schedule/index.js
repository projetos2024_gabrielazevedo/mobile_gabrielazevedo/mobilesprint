import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useState, useEffect } from 'react';
import { View, Text, TextInput, TouchableOpacity, FlatList, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';

export default function TelaAgendamento() {
  const [tituloLivro, setTituloLivro] = useState('');
  const [dataAgendada, setDataAgendada] = useState('');
  const [listaAgendamentosLivros, setListaAgendamentosLivros] = useState([]);
  const navigation = useNavigation(); // Obtenha o objeto de navegação

  useEffect(() => {
    obterListaAgendamentosLivros();
  }, []);

  const obterListaAgendamentosLivros = async () => {
    try {
      const listaAgendamentosArmazenada = await AsyncStorage.getItem('listaAgendamentosLivros');
      if (listaAgendamentosArmazenada !== null) {
        setListaAgendamentosLivros(JSON.parse(listaAgendamentosArmazenada));
      }
    } catch (error) {
      console.error('Erro ao obter lista de agendamentos de livros: ', error);
    }
  };

  const salvarAgendamento = async () => {
    try {
      const novoAgendamento = { tituloLivro, dataAgendada };
      const listaAgendamentosAtualizada = [...listaAgendamentosLivros, novoAgendamento];
      await AsyncStorage.setItem('listaAgendamentosLivros', JSON.stringify(listaAgendamentosAtualizada));
      setListaAgendamentosLivros(listaAgendamentosAtualizada);
      setTituloLivro('');
      setDataAgendada('');
      alert('Agendamento salvo com sucesso!');
    } catch (error) {
      console.error('Erro ao salvar agendamento: ', error);
    }
  };

  const removerAgendamento = async (index) => {
    try {
      const listaAtualizada = listaAgendamentosLivros.filter((_, idx) => idx !== index);
      await AsyncStorage.setItem('listaAgendamentosLivros', JSON.stringify(listaAtualizada));
      setListaAgendamentosLivros(listaAtualizada);
    } catch (error) {
      console.error('Erro ao remover agendamento: ', error);
    }
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => navigation.navigate('Home')} style={styles.homeButton}>
        <Text style={styles.homeButtonText}> ⇦ Voltar para o Início</Text>
      </TouchableOpacity>
      <Text style={styles.title}>Tela de Agendamento</Text>
      <TextInput
        style={styles.input}
        placeholder="Digite o título do livro"
        onChangeText={text => setTituloLivro(text)}
        value={tituloLivro}
      />
      <TextInput
        style={styles.input}
        placeholder="Digite a data agendada"
        onChangeText={text => setDataAgendada(text)}
        value={dataAgendada}
      />
      <TouchableOpacity style={styles.button} onPress={salvarAgendamento}>
        <Text style={styles.buttonText}>Salvar Agendamento</Text>
      </TouchableOpacity>
      <Text style={styles.subtitle}>Lista de Agendamentos de Livros:</Text>
      <FlatList
        data={listaAgendamentosLivros}
        renderItem={({ item, index }) => (
          <View style={styles.item}>
            <View style={styles.itemContent}>
              <Text style={styles.textItem}>Título: {item.tituloLivro}</Text>
              <Text style={styles.textItem}>Data Agendada: {item.dataAgendada}</Text>
            </View>
            <TouchableOpacity onPress={() => removerAgendamento(index)} style={styles.removeButton}>
              <Text style={styles.removeButtonText}>Remover</Text>
            </TouchableOpacity>
          </View>
        )}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 30,
    paddingTop: 100,
    backgroundColor: '#CAF0F8',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    color: '#023E8A',
  },
  subtitle: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 20,
    marginBottom: 10,
  },
  button: {
    backgroundColor: '#90E0EF',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 6,
    marginBottom: 10,
    width: '80%',
  },
  buttonText: {
    fontSize: 18,
    color: '#023E8A',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  homeButton: {
    position: 'absolute',
    top: 50, // Mover a seta um pouco para baixo
    left: 20,
  },
  homeButtonText: {
    color: '#023E8A',
    fontSize: 16,
    fontWeight: 'bold',
  },
  input: {
    width: '80%', // Ajustando a largura para 80% da tela
    height: 40,
    backgroundColor: 'white',
    borderColor: '#023E8A',
    borderWidth: 2,
    marginBottom: 10,
    paddingHorizontal: 10,
    borderRadius: 8, // Adicionando bordas arredondadas
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10,
    backgroundColor: '#90E0EF',
    borderRadius: 8,
    padding: 8,
    width: '100%',
  },
  itemContent: {
    flex: 1,
  },
  textItem: {
    fontSize: 18,
  },
  removeButton: {
    backgroundColor: 'red',
    paddingVertical: 8,
    paddingHorizontal: 16,
    borderRadius: 6,
  },
  removeButtonText: {
    color: 'white',
    fontWeight: 'bold',

  },
});
