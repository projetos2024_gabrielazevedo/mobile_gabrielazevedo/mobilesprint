import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';

export default function HomeScreen() {
  const navigation = useNavigation();
  return (
      <View style={styles.container}>
        <View style={styles.overlay}>
        <Image source={require('./livro.png')} style={styles.Image1} />
          <View style={styles.header}>
            <Text style={styles.title}>Bem-vindo, faça a sua reserva...</Text>
          </View>
          <View style={styles.content}>
            <Text style={styles.optionText}>Selecione uma opção:</Text>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Login')}>
              <Text style={styles.buttonText}>Login</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Cadastro')}>
              <Text style={styles.buttonText}>Cadastrar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
  );
}

const styles = StyleSheet.create({
  background: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    alignItems: 'left',
    justifyContent: 'center',
    paddingHorizontal: 20,
    backgroundColor: '#CAF0F8'
  },
  header: {
    marginBottom: 20,
  },
  Image1: {
    height: 250,
    width: 200,
    borderRadius: 20,
    position: "absolute",
    top: 10,
    transform: [
        { translateX: 70 },
        { translateY: 40 },
  
    ]
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'left',
    color: '#023E8A', // Cor do texto
    marginTop: 400,
  },
  content: {
    alignItems: 'left',
  },
  optionText: {
    fontSize: 20,
    color: '#023E8A', // Cor do texto
    marginBottom: 20,
  },
  button: {
    backgroundColor: "#90E0EF",
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 4,
    marginBottom: 10,
    width: '112%'
  },
  buttonText: {
    fontSize: 18,
    color: '#023E8A',
    textAlign: 'center',
    fontWeight: 'bold',
  },
});
