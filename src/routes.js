import React from 'react'
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from './pages/home';
import Login from './pages/login';
import Cadastro from './pages/cadastro';
import ScheduleScreen from './pages/schedule';



const Stack = createStackNavigator();

const Routes = () => {
  return (
      <Stack.Navigator initialRouteName="Home" screenOptions={{ headerShown: false}}>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Cadastro" component={Cadastro} />
        <Stack.Screen name="Schedule" component={ScheduleScreen} />
      </Stack.Navigator>
  )
}

export default Routes;